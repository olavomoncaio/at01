-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 10-Mar-2019 às 09:08
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `setor_cliente`
--

CREATE TABLE `setor_cliente` (
  `id` int(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `estado` varchar(30) NOT NULL,
  `telefone` int(20) NOT NULL,
  `celular` bigint(20) NOT NULL,
  `idade` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `setor_cliente`
--

INSERT INTO `setor_cliente` (`id`, `nome`, `email`, `cidade`, `estado`, `telefone`, `celular`, `idade`) VALUES
(3, 'Olavo Moncaio Grilenzoni', 'olavo_gh.wt@hotmail.com', 'São Paulo', 'São Paulo', 1123396346, 11988902932, 21),
(5, 'Valéria Cristina Moncaio', 'valeriacmoncaio@gmail.com', 'São Paulo', 'SP', 1123396346, 11988937293, 49),
(6, 'Joana Viena', 'joanaviena@uol.com.br', 'Curitiba ', 'Paraná', 2147483647, 42987854236, 24);

-- --------------------------------------------------------

--
-- Estrutura da tabela `setor_compra`
--

CREATE TABLE `setor_compra` (
  `id` int(20) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `celular` varchar(14) NOT NULL,
  `telefone` varchar(12) NOT NULL,
  `cep` varchar(8) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `complemento` varchar(50) NOT NULL,
  `estado` varchar(2) NOT NULL,
  `cidade` varchar(25) NOT NULL,
  `produto` varchar(25) NOT NULL,
  `preco` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `setor_compra`
--

INSERT INTO `setor_compra` (`id`, `nome`, `email`, `celular`, `telefone`, `cep`, `endereco`, `complemento`, `estado`, `cidade`, `produto`, `preco`) VALUES
(20, 'Valeria Cristina Moncaio', 'olavo_gh.wt@hotmail.com', '11988902932', '1123396346', '03646-00', 'Rua Atuaí 131', 'Apto 81', 'SP', 'São Paulo', '3', 1800),
(21, 'Thomas Vianna', 'thomasbife@gmail.com', '11988665423', '1132658987', '03654458', 'Rua São Miguel 123', '', 'SP', 'São Paulo', '10', 559),
(22, 'Valéria Cristina Moncaio', 'valeriacmoncaio@gmail.com', '11988937293', '1123396346', '03646000', 'Rua Atuai 131', 'apto 81', 'SP', 'São Paulo', '1', 1400),
(23, 'Jean Pereto', 'jeanpereto@hotmail.com', '11964587854', '1155547489', '03655000', 'Rua do Brasil 12', 'Casa 16', 'SP', 'Guarulhos', '1', 1400),
(24, 'Pamela da Silva', 'pamelasantos@hotmail.com', '119988785452', '1122363595', '03646-00', 'Rua Atuaí, 131', 'apto 12', 'SP', 'São Paulo', '16', 1650);

-- --------------------------------------------------------

--
-- Estrutura da tabela `setor_contato`
--

CREATE TABLE `setor_contato` (
  `nome` varchar(100) NOT NULL,
  `id` int(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `motivo` varchar(60) NOT NULL,
  `mensagem` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `setor_contato`
--

INSERT INTO `setor_contato` (`nome`, `id`, `email`, `motivo`, `mensagem`) VALUES
('Olavo Moncaio Grilenzoni', 1, 'olavo_gh.wt@hotmail.com', '2', 'Teste inicial'),
('Danton Ferraz', 2, 'dantonketchun@gmail.com', '3', 'Esse site está legal'),
('Marcos Castro', 3, 'marcosc@gmail.com', '4', 'Gostaria de uma oportunidade de emprego'),
('Cleber Souza', 4, 'cleber22@uol.com.br', '5', 'Gostaria de saber novidades');

-- --------------------------------------------------------

--
-- Estrutura da tabela `setor_estoque`
--

CREATE TABLE `setor_estoque` (
  `id` int(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `card_title` varchar(100) NOT NULL,
  `label` varchar(100) NOT NULL,
  `image` varchar(150) NOT NULL,
  `descr` varchar(3000) NOT NULL,
  `imgdetail` varchar(100) NOT NULL,
  `descrdetail` varchar(3000) NOT NULL,
  `preco` int(10) NOT NULL,
  `imgfinal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `setor_estoque`
--

INSERT INTO `setor_estoque` (`id`, `title`, `card_title`, `label`, `image`, `descr`, `imgdetail`, `descrdetail`, `preco`, `imgfinal`) VALUES
(1, 'Playstation 4', 'O poder e a diversão de viver em vários mundos neste console.', 'Ler mais', 'playstation4vale.jpg', 'Aventure-se em jogos novos exclusivos com amigos ou sozinho, com gráficos de última geração e nostalgia total.\r\nMelhores jogos: God of War, Persona 5, Bloodborne, Horizon: Zero Down e Spider-Man.\r\n', 'godofwar.jpg', 'O PlayStation 4, ou PS4 (como é mais conhecido) é um video game da Sony lançado em 2013. O console tem três versões principais: fat, slim e Pro. A primeira (e original) tem esse nome por causa do peso e conta com 500 GB de armazenamento. Já a edição Slim, mais leve que a anterior, pode ser encontrada com armazenamento de 500 GB ou de 1T. O console mais novo, PS4 Pro, foi lançado em 2016. Ele reproduz imagens em 4K e tem armazenamento de 1T. O preço do PS4 Pro varia entre R$ 2.000 e R$ 2.500. Enquanto o do PS4 Slim fica entre R$ 1.500 e R$ 2.000. As edições Slim e Pro têm 8GB de memória RAM GDDR5, leitor Blu-ray e sensor de movimento embutido no controle, o DualShock 4. O joystick também tem touchscreen e botão para compartilhamento. A maioria dos jogos atuais rodam no PS4, tendo versões melhoradas no PS4 Pro', 1400, 'playstation4box.jpg'),
(2, 'Xbox One', 'O poder e a evolução do sucessor do Xbox 360, o poderoso One!', 'Ler mais', 'xbox.jpg', 'Jogue mais de 1.300 jogos, incluindo mais de 200 títulos exclusivos do console e mais de 400 clássicos do Xbox. Curta o acesso ilimitado a mais de 100 jogos por um pequeno valor mensal.', 'forza.jpg', 'Aproveite a verdadeira exibição em 4K no Xbox One X, junto com player e streaming de vídeo em 4K Blu-ray™ nos dois consoles Xbox One. Jogue com a maior comunidade de jogadores na rede para jogadores mais avançada, rápida e confiável que existe', 1700, 'xboxbox.jpg'),
(3, 'Nintendo Switch', 'O Console que traz inovações para todos os gostos, principalmente para os amantes da Nintendo', 'Ler mais', 'nintendoswitch.jpg', 'O console que trouxe estreias de grandes games como The Legend of Zelda: Breath of the Wild e Super Mario Odyssey não para de inovar.', 'smashbrosultimate.jpg', 'O Switch se trata de um console híbrido semelhante a um tablet, o qual pode ser acoplado a um dock e, assim, ser transformado em um console de mesa. O console também conta com dois controles sem fio acopláveis de cada lado, chamados pela Nintendo de Joy-Con, que podem ser usados individualmente ou ser acoplados à unidade principal (no modo portátil) ou a uma base semelhante a um gamepad (no modo caseiro).\r\nRumores recentes prometem novidades grandiosas para o console, incluindo uma possível parceria com a Microsoft.', 1800, 'switchbuy.jpg'),
(7, 'Game Boy Advance', 'Os amantes de Pokémon amam, e a nostalgia desse maravilhoso Game Boy atrai qualquer um', 'Ler mais', 'gameboyadv.jpg', 'O Game Boy Advance (popularmente abreviado como GBA) é um console portátil desenvolvido e fabricado pela empresa japonesa Nintendo. Lançado em 2001, é o sucessor do Game Boy Color e um dos últimos produtos da linha Game Boy. Seu codinome durante o processo de desenvolvimento foi Advance Game Boy. Ele continuou recebendo jogos até 2008.', 'pok.jpg', 'O Game Boy Advance é retrocompatível com os jogos lançados para o Game Boy e o Game Boy Color. Devido a resolução de tela da geração anterior será apresentado tarjas pretas, sendo que é possível utilizar os botões \"L\" e \"R\" para alterar entre a escala original e estendida, ocupando toda a tela.<br>\r\nO Game Boy Advance foi vendido a um preço de $100 USD na época de seu lançamento na América do Norte. Até o lançamento do Game Boy Advance SP, o GBA foi o console com as vendas mais rápidas da história. Apesar de seu sucesso, muitos criticaram o modelo original do Game Boy Advance por ainda não ter adotado uma tela com iluminação própria, o que a Nintendo iria consertar em modelos mais avançados. Em 2005 o modelo original do GBA deixou de ser produzido.', 350, 'linkinho.jpg'),
(10, 'Nintendo 64', 'O aclamado clássico na Nintendo, que marcou gerações', 'Ler mais', 'n64.jpg', 'Embora os consoles concorrentes PlayStation e Sega Saturn tenham recebido mais jogos (cerca de 1.100 e 600 jogos, respectivamente), assim como os consoles anteriores da própria Nintendo (SNES com cerca de 725; NES com cerca de 768), o Nintendo 64 possui um grande número de jogos aclamados pela crítica e com muitas vendas. Super Mario 64 foi o jogo mais vendido da geração.', 'bowser.jpg', 'Lançado em 23 de junho de 1996 no Japão, contava com três títulos de lançamento disponíveis: Super Mario 64, PilotWings 64 e Saikyou Habu Shogi. Nos EUA e no Brasil foi lançado simultaneamente em 29 de setembro de 1996, e, em ambos os países, havia apenas dois títulos de lançamento disponíveis: Super Mario 64 e PilotWings; mas, equanto nos EUA os títulos eram vendidos à parte, no Brasil, o console vinha com o cartucho de Super Mario 64 incluso. Já quando foi lançado na Europa, em 1 de março de 1997, o console contou com sete jogos de lançamento: além de Super Mario 64 e PilotWings, havia Wayne Gretzky\'s 3D Hockey, Cruis\'n USA, Star Wars: Shadows of the Empire, FIFA Soccer 64 e Turok: Dinosaur Hunter.\r\n\r\nO Nintendo 64 foi o último grande console doméstico a utilizar cartucho até o Nintendo Switch, lançado em 2017.\r\n\r\nNo Brasil, foi lançado oficialmente pela Playtronic, e, a partir de 1997, pela empresa Gradiente Eletrônicos, que assumiu a representação da Nintendo no país. Em Portugal, foi distribuída pela Concentra.\r\n\r\nO console foi anunciado em 1993 com o codename \"Project Reality\", com plano de lançamento para arcades em 1994 e uma versão doméstica no ano seguinte. Em 1995, fora primeiro apresentado com o nome Nintendo Ultra 64, tendo o nome reduzido para Nintendo 64 em fevereiro de 1996 (5 meses antes do lançamento). Seu código de modelo é NUS-001 (cuja sigla significa Nintendo Ultra Sixty Four - o codinome do projeto).', 559, 'n64buy.jpg'),
(16, 'Nintendo 3DS XL', 'A evolução do Nintendo DS ganhou uma nova forma, o 3DS XL', 'Ler mais', '3ds.jpg', 'O Nintendo 3DS, quando lançado, surpreendeu muito, não apenas os fãs da Nintendo, quanto outros players. Com a evolução dele para 3DS XL, o tamanho da tela e a atratividade cresceram muito!', '3dsgame.jpg', 'O Nintendo 3DS, abreviado como 3DS, é um console portátil produzido pela Nintendo e lançado em 2011, sucessor do Nintendo DS. Tem a capacidade de produzir imagens em três dimensões sem óculos especiais e é compatível com títulos dos antecessores Nintendo DS e DSi. <br>\r\nO portátil tem um direcional analógico localizado acima do direcional digital e tela superior de 3,5 polegadas com resolução de 800x240 píxeis em modo normal e 400x240 (WQVGA) com o 3D ativado e tela inferior com a resolução de 320x240 pixels (QVGA) , além de um acelerômetro e um giroscópio. Os primeiros detalhes foram revelados durante a Electronic Entertainment Expo de junho de 2010. Tanto no Brasil como em Portugal, foi o primeiro console da empresa a ser integralmente traduzido ao português local.<br>\r\nO Nintendo 3DS usa uma tela com tecnologia que permite ver imagens em 3D sem óculos, que envia duas imagens com ângulos ligeiramente diferentes para cada olho, causando um efeito chamado auto estereoscopia. Esse efeito ocorre quando enxergamos duas imagens idênticas ligeiramente deslocadas, porem em pontos estratégicos. Ao juntar essas duas imagens, o cérebro forma uma terceira imagem, a qual nos dará a sensação de tridimensionalidade', 1650, '3dsbox.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `setor_cliente`
--
ALTER TABLE `setor_cliente`
  ADD PRIMARY KEY (`id`,`email`);

--
-- Indexes for table `setor_compra`
--
ALTER TABLE `setor_compra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setor_contato`
--
ALTER TABLE `setor_contato`
  ADD PRIMARY KEY (`id`,`email`);

--
-- Indexes for table `setor_estoque`
--
ALTER TABLE `setor_estoque`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `setor_cliente`
--
ALTER TABLE `setor_cliente`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `setor_compra`
--
ALTER TABLE `setor_compra`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `setor_contato`
--
ALTER TABLE `setor_contato`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `setor_estoque`
--
ALTER TABLE `setor_estoque`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
