<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EstoqueModel extends CI_Model {

    public function get_data($id){
       $sql = "SELECT * FROM setor_estoque WHERE id = $id";
       $res = $this->db->query($sql);
       $data = $res->result_array();
       return $data[0];
    }

    public function novo_setor(){
        if(sizeof($_POST) == 0) return;
        
        $data = $this->input->post();
        $this->db->insert('setor_estoque', $data);

    }

    public function nova_mensagem(){
        if(sizeof($_POST) == 0) return;
        
        $data = $this->input->post();
        $this->db->insert('setor_contato', $data);

    }

    public function novo_cliente(){
        if(sizeof($_POST) == 0) return;
        
        $data = $this->input->post();
        $this->db->insert('setor_cliente', $data);

    }

    public function nova_compra(){
        if(sizeof($_POST) == 0) return;
        
        $data = $this->input->post();
        $this->db->insert('setor_compra', $data);

    }

   public function editar_setor($id){
        $newdata = $this->input->post();

        if($newdata == null) return;
        $this->db->update('setor_estoque', $newdata, array('id' => $id));
    } 

    public function delete_setor($id){
        $this->db->where('id', $id);
        $this->db->delete('setor_estoque');
    }

    

}

?>
