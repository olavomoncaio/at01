<?php
    class Estoque extends CI_Controller{
        public function index(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('intro');
            $this->load->view('common/footer');
            $this->load->view('common/rodape');
        }
   
        public function setor($id){
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('EstoqueModel');
            $data = $this->EstoqueModel->get_data($id);
            $v['titulo1'] = $this->load->view('produtos/label_setor', $data, true);
            $v['setor1'] = $this->load->view('produtos/setor1', $data, true);
            $v['figure'] = $this->load->view('produtos/figure', $data, true);
            $v['form1'] = $this->load->view('produtos/form1', '', true);

            $this->load->view('produtos/layout_setor', $v);
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function detalhes($id){
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('EstoqueModel');
            $data = $this->EstoqueModel->get_data($id);
            $v['titulo1'] = $this->load->view('produtos/label_setor', $data, true);
            $v['setor2'] = $this->load->view('produtos/setor2', $data, true);
            $v['figure'] = $this->load->view('produtos/figure', $data, true);


            $this->load->view('produtos/layout_detalhes', $v);
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
            
        }

        public function fale_conosco(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('EstoqueModel');
            $this->EstoqueModel->nova_mensagem();

            $this->load->view('common/contato');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function cadastrar(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('EstoqueModel');
            $this->EstoqueModel->novo_cliente();

            $this->load->view('common/cliente');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function comprar($id){
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('EstoqueModel');
            $data = $this->EstoqueModel->get_data($id);
            $v['titulo1'] = $this->load->view('produtos/label_setor', $data, true);
            $v['setor1'] = $this->load->view('produtos/setor1', $data, true);
            $v['figure'] = $this->load->view('produtos/figure', $data, true);
            $v['form1'] = $this->load->view('produtos/form1', '', true);
            $this->EstoqueModel->nova_compra();

            $this->load->view('common/compra');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function quem_somos(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('common/quem_somos');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }   


        public function cria_setor(){
            $this->load->view('common/header');
            $this->load->view('common/navbaradm');

            $this->load->model('EstoqueModel');
            $this->EstoqueModel->novo_setor();

            $this->load->view('produtos/form_cria_setor');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function editar($id){
            $this->load->view('common/header');
            $this->load->view('common/navbaradm');

            $this->load->model('EstoqueModel');
            $this->EstoqueModel->editar_setor($id);
            $data = $this->EstoqueModel->get_data($id);

            $this->load->view('produtos/form_cria_setor', $data);
            $this->load->view('common/rodape');
            $this->load->view('common/footer');

        }

        public function delete($id){
            $this->load->view('common/header');
            $this->load->view('common/navbaradm');

            $this->load->model('EstoqueModel');
            
            $data = $this->EstoqueModel->get_data($id);
            $v['titulo1'] = $this->load->view('produtos/label_setor', $data, true);
            $v['setor1'] = $this->load->view('produtos/setor1', $data, true);
            $v['figure'] = $this->load->view('produtos/figure', $data, true);
            $v['form1'] = $this->load->view('produtos/form1', '', true);
            $this->load->view('common/confirm');

            $this->load->view('common/rodape');
            $this->load->view('common/footer');

        }

        public function adm(){
            $this->load->view('common/header');
            $this->load->view('common/navbaradm');

            $this->load->view('common/admin.php');
            
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

    

        
       

    }


?>