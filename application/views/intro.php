

<div class="card card-image" style="background-image: url(assets/img/consoles.jpg);">
  <div class="text-white text-center rgba-stylish-strong py-5 px-4">
    <div class="py-5">

      
      <h5 class="h5 orange-text"><i class="fas fa-gamepad"></i> Felix Consoles <i class="fas fa-cat"> </i></h5>
      <p class="mb-4 pb-2 px-md-5 mx-md-5">Os melhores preços para jogos, consoles e suporte.</p>
      <p class="h2 mb-4 pb-2 px-md-5 mx-md-5"><i class="fab fa-playstation"></i> / <i class="fab fa-xbox"></i></p>
      <p class="h2 mb-4 pb-2 px-md-5 mx-md-5"><i class="fab fa-nintendo-switch"></i> / <i class="fab fa-steam"></i></p>
      <a href="estoque/setor/1" class="btn peach-gradient"><i class="fas fa-clone left"></i>Clique para redirecionar!</a>

    </div>
  </div>
</div>
