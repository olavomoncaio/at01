<?php
  include_once("conexao.php");
?>
<nav class="navbar navbar-expand-lg navbar-dark primary-color">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">

    <span class="navbar-toggler-icon"></span>
    </button>
     
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('')?>">Home
          <span class="sr-only"></span>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Clientes</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="<?= base_url('estoque/cadastrar')?>">Cadastre-se</a>
          <a class="dropdown-item" href="<?= base_url('estoque/fale_conosco')?>">Fale Conosco</a>
        </div>
      </li>
    
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Produtos</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
        <?php
          $result_titulos = "SELECT * FROM setor_estoque";
          $resultado_titulos = mysqli_query($conn, $result_titulos);
          while($row_titulo = mysqli_fetch_assoc($resultado_titulos)){

            echo "<a class='dropdown-item' href='http://localhost/atividade/estoque/setor/$row_titulo[id]'> ". $row_titulo['title'] ." </a>";
            
          }

        ?>
        </div>
      </li>

      
     <li class="nav-item">
        <a class="nav-link" href="<?= base_url('estoque/quem_somos')?>">Quem somos
          <span class="sr-only"></span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('estoque/adm')?>">Admin
          <span class="sr-only"></span>
        </a>
      </li>

    </ul>

  </div>


</nav>
