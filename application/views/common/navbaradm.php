<?php
  include_once("conexao.php");
?>
<nav class="navbar navbar-expand-lg navbar-dark primary-color">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">

    <span class="navbar-toggler-icon"></span>
    </button>
     
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('')?>">Home
          <span class="sr-only"></span>
        </a>
      </li>
    
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('estoque/cria_setor')?>">Criar setor
          <span class="sr-only"></span>
        </a>
      </li>


    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Editar Produto</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
        <?php
          $result_titulos = "SELECT * FROM setor_estoque";
          $resultado_titulos = mysqli_query($conn, $result_titulos);
          while($row_titulo = mysqli_fetch_assoc($resultado_titulos)){

            echo "<a class='dropdown-item' href='http://localhost/atividade/estoque/editar/$row_titulo[id]'> ". $row_titulo['title'] ." </a>";
            
          }

        ?>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Apagar Produto</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
        <?php
          $result_titulos = "SELECT * FROM setor_estoque";
          $resultado_titulos = mysqli_query($conn, $result_titulos);
          while($row_titulo = mysqli_fetch_assoc($resultado_titulos)){

            echo "<a class='dropdown-item' href='http://localhost/atividade/estoque/delete/$row_titulo[id]'> ". $row_titulo['title'] ." </a>";
            
          }

        ?>
        </div>
      </li>


    </ul>

  </div>


</nav>
