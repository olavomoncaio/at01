
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <form method="GET" class="text-center border border-light p-5">

                <p class="h4 mb-4">Exclusão do setor <?=$id?> / <?=$title?></p>

                <label>Você realmente deseja excluir?</label>
                <select class="browser-default custom-select mb-4" id="opc" name="opc">

                    <option value="" disabled>Escolha uma opção:</option>
                    <option value="1" selected >Sim</option>
                    <option value="2">Não</option>

                </select>


                <button class="btn btn-info btn-block" type="submit">Enviar</button>

<?php
    echo "<a href='http://localhost/atividade/estoque/adm' class='btn btn-success'>Voltar</a>";

    if ( isset($_GET['opc'])) {
        $opc = $_GET['opc'];
        
        

        if ($opc == 1){
            $this->EstoqueModel->delete_setor($id);
            echo "<p>O setor foi excluido com sucesso!</p>";
        }
        if ($opc == 2){
            echo "<p>O setor não foi excluido!</p>";
        }
        
        
	} 
    

?>
</form>
        </div>
    </div>
</div> 