<div class="container">
    <div class="row">
        <div class="col-md-6">
            <form method="POST" class="text-center border border-light p-5">

            <p class="h4 mb-4">Obrigado pela escolha!</p>

            <input type="text" id="nome" name="nome" class="form-control mb-4" placeholder="Nome">

            <select class="browser-default custom-select mb-4" id="produto" name="produto">
                <option value="<?= $id ?>" selected><?= $title ?></option>
            </select>

            <select class="browser-default custom-select mb-4" id="preco" name="preco">
                <option value="<?= $preco ?>" selected>R$ <?= $preco ?>,00</option>
            </select>

            <input type="text" id="email" name="email" class="form-control mb-4" placeholder="E-mail">

            <input type="text" id="cidade" name="cidade" class="form-control mb-4" placeholder="Cidade">

            <input type="text" id="estado" name="estado" class="form-control mb-4" placeholder="Estado (Exemplo: XX)">

            <input type="text" id="telefone" name="telefone" class="form-control mb-4" placeholder="Telefone">

            <input type="text" id="celular" name="celular" class="form-control mb-4" placeholder="Celular">

            <input type="text" id="cep" name="cep" class="form-control mb-4" placeholder="Cep (Exemplo: 00000-000)">

            <input type="text" id="endereco" name="endereco" class="form-control mb-4" placeholder="Endereço">

            <input type="text" id="complemento" name="complemento" class="form-control mb-4" placeholder="Complemento">

            <button class="btn btn-info btn-block" type="submit">Enviar</button>

            </form>
         </div>
        <div class="col-md-2">
        </div>

        <div class="col-md-4">
            <br />
            <img src="http://localhost/atividade/assets/img/link.jpg" width="600px">
            <p>
                <h4>Cadastre-se para obter promoções exclusivas via e-mail!</h4>
            </p>
            <p>
                <h4>Não deixe de entrar em contato caso ainda tenha dúvidas sobre o produto.</h4>
            </p>
    
         </div>
    </div>
</div>