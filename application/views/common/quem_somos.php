
<div class="container" style="background-color: white;"><br>
    <div class="row">
        <div class="col-md-8">
            <img src="http://localhost/atividade/assets/img/consoles.jpg" width="1100px"/>
        </div>
        <div class="col-md-12">
           <p class="card-text" style="font-size: 18px; text-align: justify;"> <br>
                Nós da Felix Consoles estamos no mercado de consoles desde 2018, vendendo e oferencendo a nossos clientes
                os melhores preços e confiabilidade do mercado de games. <br><br>
                4 amigos fãs de jogos e técnologia decidiram iniciar um negócio próprio, que unisse tanto a rentabilidade
                quanto os prazeres e gostos de cada um deles... E foi assim que nós surgimos. Em meio a uma crise econômica
                nacional, se estabelecer no mercado não foi algo fácil e simples de inicio, porém correr atrás dos sonhos
                é algo obrigatório para quem deseja perseverar nessa vida. <br><br>
                Com a criação inicial da loja virtual, e visto o sucesso obtido, a primeira loja foi aberta em Novembro de
                2018, na cidade de São Paulo. Como lá é um grande centro, as vendas sempre continuaram e exponencial crescimento.
                A Felix consoles tem orgulho e muita gratidão, pois graças a seus clientes, está loja não para de crescer, assim
                podendo oferecer melhores produtos e satisfação aos fãs de games. <br><br>
                A venda de consoles, que foi e ainda é nosso foco, será um pouco alterada, pois futuramente a também produziremos
                fliperamas portáteis, que vão possibilitar os jogadores de jogar coisas antigas a mais nostálgica, como jogos
                do Super Nintendo, Nintendinho, Playstation 1, Megadrive, Master System e Atari! <br><br>
                A comercialização de jogos também é algo que está em nossa mente, futuramente possuiremos em nossos estoques
                jogos de todos os consoles da geração atual, geração passada e gerações futuras, para podermos assim preencher
                ainda mais os desejos dos nossos clientes. <br><br>
            </p>
        </div>

    </div>
</div><br>
