
<footer class="page-footer font-small cyan darken-3">

    
    <div class="container">

      <div class="row">

       
        <div class="col-md-12 py-5">
          <div class="mb-5 flex-center">

            
            <a href="https://www.facebook.com/Felix-Consoles-1141258419377462/" target="_blank" class="fb-ic">
              <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
        
            <a href="https://twitter.com/ConsolesFelix" target="_blank" class="tw-ic">
              <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
            
            <a href="https://www.instagram.com/felixconsoles/" target="_blank" class="ins-ic">
              <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>

            <div class="row text-center">
            <div class="col-md-4">
              <a class="btn-floating blue accent-1">
                <i class="fas fa-map-marker-alt"></i>
              </a>
              <p>São Paulo, Av. Paulista 2357</p>
              <p class="mb-md-0">Brasil</p>
            </div>
            <div class="col-md-4">
              <a class="btn-floating blue accent-1">
                <i class="fas fa-phone"></i>
              </a>
              <p>(11)98890-2932</p>
              <p class="mb-md-0">Segunda - Sexta, 9:00-18:00</p>
            </div>
            <div class="col-md-4">
              <a class="btn-floating blue accent-1">
                <i class="fas fa-envelope"></i>
              </a>
              <p>contato@felixconsoles.com.br</p>
              <p class="mb-0">vendas@felixconsoles.com.br</p>
            </div>
          </div>
          </div>
        </div>
     </div>
    

    </div>
    <div class="footer-copyright text-center py-3">© 2019 Copyright: Felix Consoles Ltda.
    </div>


  </footer>
