<br>
<div class="card">
  <img class="card-img-top" src="http://localhost/atividade/assets/img/<?= $imgdetail ?>" alt="Console">
  <div class="card-body">

    <h4 class="card-title"><a><?= $title ?></a></h4>
    
    
    <p class="card-text" style="text-align: justify; font-size: 18px;"><?= $descrdetail ?>.</p>

    <div class="container" style="text-align: center;">
        <img src="http://localhost/atividade/assets/img/<?= $imgfinal ?>" alt="Console" width="750px">
    </div>
    
    <p class="card-text" style="text-align: justify; font-size: 13px;">A Felix Consoles proporciona a seus clientes total garantia de 1 ano para defeitos
                                                                       e problemas futuros que o produto possa vir a apresentar, cobrindo todos os gasto relacionados
                                                                       a isso. (Não inclusos danos por uso indevido ou descuidado)
    </p>

    <a href="http://localhost/atividade/estoque/comprar/<?=$id ?>" class="btn btn-success">Comprar - R$ <?= $preco ?>,00</a>
    <a href="http://localhost/atividade/estoque/fale_conosco" class="btn btn-danger">Dúvidas? Clique aqui</a>


  </div>

</div>
