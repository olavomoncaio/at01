<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto mt-3">

        <form method="POST" class="text-center border border-light p-5">
        <p class="h4 mb-4">Crie uma nova página de produto e detalhes no site</p>
        <input type="text" value="<?= isset($title) ? $title : '' ?>" id="title" name="title" class="form-control mb-4" placeholder="Console ou Jogo">
        <input type="text" value="<?= isset($image) ? $image : '' ?>" id="image" name="image" class="form-control mb-4" placeholder="Nome da imagem (Exemplo: nintendowii.jpg)">
        <input type="text" value="<?= isset($card_title) ? $card_title : '' ?>" id="card_title" name="card_title" class="form-control mb-4" placeholder="Título inicial da imagem">
        <div class="form-group">
            <textarea class="form-control rounded-0" id="descr" name="descr" rows="6" placeholder="Descrição inicial"><?= isset($descr) ? $descr : '' ?></textarea>
        </div>
        <input type="text" value="<?= isset($label) ? $label : '' ?>" id="label" name="label" class="form-control mb-4" placeholder="Rótulo do botão">
        
        
        <!--Página de detalhes -->
        <input type="text" value="<?= isset($imgdetail) ? $imgdetail : '' ?>" id="imgdetail" name="imgdetail" class="form-control mb-4" placeholder="Figura inicial dos detalhes">
        <div class="form-group">
            <textarea class="form-control rounded-0" id="descrdetail" name="descrdetail" rows="6" placeholder="Descrição"><?= isset($descrdetail) ? $descrdetail : '' ?></textarea>
        </div>
        <input type="number" value="<?= isset($preco) ? $preco : '' ?>" id="preco" name="preco" class="form-control mb-4" placeholder="Preço do Produto (Exemplo: 1200)">
        <input type="text" value="<?= isset($imgfinal) ? $imgfinal : '' ?>" id="imgfinal" name="imgfinal" class="form-control mb-4" placeholder="Imagem do final (Exemplo: nintendogames.jpg)">

        
        <button class="btn btn-info btn-block" type="submit">Publicar</button>

        </form>
        
        </div>
    </div>
</div>