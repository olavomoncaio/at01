<div class="card">

  <img class="card-img-top" src="http://localhost/atividade/assets/img/<?= $image ?>" alt="Console">

  <div class="card-body">

    <h4 class="card-title" style="text-align: center;"><a><?= $card_title ?></a></h4>
    <div class="col-md-6">
      <p class="card-text" style="text-align: justify;"><?= $descr ?></p>
    </div>
    <br>
    <a href="http://localhost/atividade/estoque/detalhes/<?= $id ?>" class="btn btn-primary"><?= $label ?></a>

  </div>

</div>
