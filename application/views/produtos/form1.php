<form class="text-center border border-light p-5">

    <p class="h4 mb-4">Fale conosco</p>

    <input type="text" id="defaultContactFormName" class="form-control mb-4" placeholder="Nome">

    <input type="email" id="defaultContactFormEmail" class="form-control mb-4" placeholder="E-mail">

    <label>Motivo do contato</label>
    <select class="browser-default custom-select mb-4">
        <option value="" disabled>Escolha uma opção:</option>
        <option value="1" selected>Dúvidas</option>
        <option value="2">Elogios ou reclamações</option>
        <option value="3">Reportar algum erro</option>
        <option value="4">Entrar na equipe Felix</option>
    </select>

    <div class="form-group">
        <textarea class="form-control rounded-0" id="exampleFormControlTextarea2" rows="3" placeholder="Digite aqui..."></textarea>
    </div>

    <button class="btn btn-info btn-block" type="submit">Enviar</button>

</form>
